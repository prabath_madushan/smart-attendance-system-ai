import React from "react";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";

import WelcomePage from "./containers/welcome-page/WelcomePage";
const theme = createMuiTheme({});
function App() {
  return (
    <ThemeProvider theme={theme}>
      <WelcomePage />
    </ThemeProvider>
  );
}

export default App;
