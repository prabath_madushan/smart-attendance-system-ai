import React, { useState, useEffect } from "react";
import * as faceapi from "face-api.js";

const FaceRecognition = () => {
  const [state, setState] = useState({
    loading: true,
    faceCount: 0,
  });

  useEffect(() => {
    Promise.all([
      faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
      //   faceapi.nets.tinyFaceDetector.loadFromUri("/models"),
      faceapi.nets.faceLandmark68Net.loadFromUri("/models"),
      //   faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
      faceapi.nets.ssdMobilenetv1.loadFromUri("/models"),
    ])
      .then(() => {
        setState((ps) => {
          return { ...ps, loading: false };
        });
        start();
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const start = () => {
    console.log("started");
  };

  const onUploadImage = async (file) => {
    setImage(URL.createObjectURL(file));
    const image = await faceapi.bufferToImage(file);
    const detections = await faceapi
      .detectAllFaces(image)
      .withFaceLandmarks()
      .withFaceDescriptors();
    setState({ ...state, faceCount: detections.length });
  };

  const [image, setImage] = useState(null);
  return (
    <div>
      {state.loading ? (
        <label>loading...</label>
      ) : (
        <form>
          <h1>{state.faceCount}</h1>
          <div>
            <input
              type="file"
              onChange={async (e) => onUploadImage(e.target.files[0])}
            />
          </div>
          <div>
            <img src={image} alt="" />
          </div>
        </form>
      )}
    </div>
  );
};

export default FaceRecognition;
