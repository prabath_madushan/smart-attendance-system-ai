import React, { useRef, useEffect } from "react";
import * as faceapi from "face-api.js";
import "./App.css";

function FaceDetection() {
  const video = useRef(null);
  useEffect(() => {
    Promise.all([
      faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
      faceapi.nets.tinyFaceDetector.loadFromUri("/models"),
      faceapi.nets.faceLandmark68Net.loadFromUri("/models"),
      faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
      faceapi.nets.faceExpressionNet.loadFromUri("/models"),
    ])
      .then(() => {
        navigator.getUserMedia(
          { video: {} },
          (stream) => (video.current.srcObject = stream),
          (err) => console.log(err)
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, [video]);

  async function load() {}

  const onPlay = () => {
    const displaySize = {
      width: video.current.width,
      height: video.current.height,
    };
    const canvas = faceapi.createCanvasFromMedia(video.current);
    document.getElementById("container").append(canvas);
    faceapi.matchDimensions(canvas, displaySize);
    setInterval(async () => {
      const detection = await faceapi
        .detectAllFaces(video.current, new faceapi.TinyFaceDetectorOptions())
        .withFaceLandmarks()
        .withFaceExpressions();

      console.log(detection);
      const resizeDetactions = faceapi.resizeResults(detection, displaySize);
      canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
      faceapi.draw.drawDetections(canvas, resizeDetactions);
      faceapi.draw.drawFaceLandmarks(canvas, resizeDetactions);
      faceapi.draw.drawFaceExpressions(canvas, resizeDetactions);
    }, 200);
  };

  return (
    <div id="container">
      <video
        ref={video}
        width="720"
        height="560"
        autoPlay
        muted
        onPlay={onPlay}
      ></video>
    </div>
  );
}

export default FaceDetection;
